import sys
import re
from copy import deepcopy
import os
import time
from subprocess import Popen, PIPE
from operations import *

operation = ["concat", "_ extract", "bvnot", "bvand", "bvor", "bvneg", "bvadd", "bvmul", "bvudiv", "bvurem", "bvshl",
             "bvlshr", "bvult", "bvnand", "bvnor", "bvxnor", "bvcomp", "bvsub", "bvsdiv", "bvsrem", "bvsmod", "bvashr",
             "_ repeat", "_ zero_extend", "_ sign_extend", "_ rotate_left", "_ rotate_right i", "bvule", "bvugt",
             "bvuge", "bvslt", "bvsle", "bvsgt", "bvsge", "bvnand", "bvnor", "bvxor", "=", "let", 'ite', "=>", "not",
             "or", "and", "let"]

# {variableName:(listofSegments, length of origin variable)}
variableMap = {}
lengthMap = {}
boolList = []
letVariable = []
constantValue = {}
printAllowed = False
define_bools = []
defines = []
declare_bools = []
declares = []


def tryRunWobit():
    p = Popen(["stp_simple", sys.argv[1]], stdout=PIPE)
    output, err = p.communicate()
    if re.compile(b"\d+.\d+,").match(output) is not None:
        print(output.rsplit(b"\n")[0].decode("utf-8"))
        sys.exit()


def readClauses(smtFile):
    leftCount = 0
    rightCount = 0
    startPosition = 0
    inComment = False
    # todo: deal with '\n' in test case like 'test.smt2'
    clauseList = []
    for index, smt in enumerate(smtFile):
        if inComment is False:
            if smt == ";":
                inComment = True
            elif smt == '(':
                if leftCount == 0:
                    leftCount += 1
                    startPosition = index
                else:
                    leftCount += 1
            elif smt == ')':
                if rightCount - leftCount == -1:
                    endPosition = index
                    sliceString = smtFile[startPosition: endPosition + 1]
                    clauseList.append(sliceString)
                    leftCount = 0
                    rightCount = 0
                else:
                    rightCount += 1
        elif smt == "\n":
            inComment = False
    return clauseList


def checkType(clause):
    infoPattern = re.compile("^\(\s*set-info")
    logicPattern = re.compile("^\(\s*set-logic")
    declarePattern = re.compile("^\(\s*declare-fun")
    definePattern = re.compile("^\(\s*define-fun")
    assertPattern = re.compile("^\(\s*assert")
    checkPattern = re.compile("^\(\s*check-sat")
    exitPattern = re.compile("^\(\s*exit")
    if infoPattern.match(clause) is not None:
        return "info"
    if logicPattern.match(clause) is not None:
        return "logic"
    if declarePattern.match(clause) is not None:
        return "declare"
    if definePattern.match(clause) is not None:
        return "define"
    if assertPattern.match(clause) is not None:
        return "assert"
    if checkPattern.match(clause) is not None:
        return "check"
    if exitPattern.match(clause) is not None:
        return "exit"


def getDeclareFun(clauseList):
    global declares
    global declare_bools
    for clause in clauseList:
        if checkType(clause) == 'declare':
            m = re.match("^(\(\s*declare-fun\s*)(\S*)(\s*)(\(\))(\s*)(\(\s*_\s*BitVec\s*)(\d+)", clause)
            if m is not None:
                atom = (m.group(2), m.group(7))
                declares.append(atom)
            m = re.match("^(\(\s*declare-fun\s*)(\S*)(\s*)(\(\))(\s*)(Bool)", clause)
            if m is not None:
                bools = (m.group(2), m.group(6))
                boolList.append(bools)
                declare_bools.append(bools)


def getDefineFun(clauseList):
    global defines
    global define_bools
    for clause in clauseList:
        if checkType(clause) == 'define':
            m = re.match("^(\(\s*define-fun\s*)(\S*)(\s*)(\(.*\))(\s*)(\(\s*_\s*BitVec\s*(\d+)\))\s+(\(.+\))\)$",
                         clause)
            if m is not None:
                atom = (m.group(2), m.group(7))
                if m.group(4) != '()':
                    break
                # print("not ()", m.group(4))
                #     m1 = re.match("\(\((\S+)\s*\(_\s*BitVec\s*(\d+)\)\)\)", m.group(4))
                #     if int(m1.group(2)) < 64:
                #         print("length < 64", m1.group(1), m1.group(2))
                #         declares.append((m1.group(1), m1.group(2)))
                #     print(declares)
                # print("atom", atom)
                exp = parenthesis_split(m.group(8))
                exp = list(filter(lambda a: a != '', exp))
                scanAndTransform(exp)
                length, transform = readAssert(exp[0])
                # print("exp", exp, length, transform)
                if not transform:
                    defines.append(atom)
                else:
                    variableMap.update({m.group(2): (exp[0], length)})
            m = re.match("^(\(\s*define-fun\s*)(\S*)(\s*)(\(\))(\s*)(Bool)(\s*)(\(.+\))\)$", clause)
            if m is not None:
                atom = m.group(2)
                exp = parenthesis_split(m.group(8))
                exp = list(filter(lambda a: a != '', exp))
                scanAndTransform(exp)
                length, transform = readAssert(exp[0])
                if not transform:
                    define_bools.append((atom, exp[0]))
                    boolList.append((atom, m.group(6)))
                else:
                    variableMap.update({atom: (exp[0], length)})
                    boolList.append((exp, m.group(6)))


def parenthesis_split(sentence, separator=" ", lparen="(", rparen=")"):
    nb_brackets = 0
    # print("origin sentence", sentence)
    sentence = sentence.strip(separator)  # get rid of leading/trailing seps
    # print("sentence", sentence)
    l = [0]
    for i, c in enumerate(sentence):
        if c == lparen:
            nb_brackets += 1
        elif c == rparen:
            nb_brackets -= 1
        elif c == separator and nb_brackets == 0:
            l.append(i)
        # handle malformed string
        if nb_brackets < 0:
            raise Exception("Syntax error")

    l.append(len(sentence))
    # handle missing closing parentheses
    if nb_brackets > 0:
        raise Exception("Syntax error")

    return [sentence[i:j].strip(separator) for i, j in zip(l, l[1:])]


def getAsserts(clauseList):
    assertLists = []
    for clause in clauseList:
        if checkType(clause) == 'assert':
            if '\n' not in clause:
                m = re.match("(\(\s*assert\s*(\((.*)\))\s*\))", clause)
                assertContent = m.group(3).strip()  # = c (bvadd a b)
            else:
                m = re.match("^(\(assert)((.|\n)+)\)$", clause)
                assertContent = m.group(2).replace('\n', ' ')
            assertList = parenthesis_split(assertContent)
            assertList = list(filter(lambda a: a != '', assertList))
            scanAndTransform(assertList)
            assertLists.append(assertList)
    return assertLists


def getVariableList(variable):
    variableList = []
    v = variable[0]
    bits = int(variable[1])
    l = int(math.ceil(float(bits) / 64))
    for i in range(0, l):
        variableList.append(v + '_segment_' + str(i))
        if i == 0 and bits % 64 > 0:
            declares.append((v + '_segment_' + str(i), str(bits % 64)))
        else:
            declares.append((v + '_segment_' + str(i), '64'))
    return variableList, bits


def mapVariables():
    removeList = []
    for variable in declares:
        if int(variable[1]) > 64:
            variableTuple = getVariableList(variable)
            variableMap.update({variable[0]: variableTuple})
            removeList.append(variable)
    for rl in removeList:
        declares.remove(rl)


def checkAtom(str):
    m = re.match("^\(", str)
    if m is not None:
        return False
    else:
        return True


def checkAllAtom(strList):
    for element in strList:
        if isinstance(element, list):
            if not checkAllAtom(element):
                return False
        elif not checkAtom(element):
            return False
    return True


def scanAndTransform(assertList):
    while not checkAllAtom(assertList):
        for index, element in enumerate(assertList):
            if isinstance(element, list):
                scanAndTransform(element)
            elif not checkAtom(element):
                assertList[index] = parenthesis_split(element[1:-1])


def fetchTwoArguments(clause, a, b):
    # print("origin second", clause[b])
    first = clause[a]
    firstLength = -1
    second = clause[b]
    secondLength = -1
    firstIte = False
    secondIte = False
    if not isinstance(first, list):
        if first == "false" or first == "true":
            firstLength = 0
        elif first in dict(declare_bools):
            firstLength = 0
        elif first in dict(define_bools):
            firstLength = 0
        elif first in variableMap:
            clause[a], firstLength = deepcopy(variableMap.get(first))
        elif first in dict(letVariable):
            clause[a] = [clause[a]]
            firstLength = dict(letVariable)[first]
        elif first in dict(declares):
            clause[a] = [clause[a]]
            firstLength = int(dict(declares)[first])
        else:
            clause[a] = [clause[a]]
            firstLength = int(dict(defines)[first])
    else:
        readAssert(first)
        if first in boolList:
            firstLength = 0
    if not isinstance(second, list):
        if second == "false" or second == "true":
            secondLength = 0
        elif second in dict(declare_bools):
            # print("dcb")
            secondLength = 0
        elif second in dict(define_bools):
            # print("dfb")
            secondLength = 0
        elif second in variableMap:
            # print("var")
            clause[b], secondLength = deepcopy(variableMap.get(second))
        elif second in dict(letVariable):
            # print("let")
            clause[b] = [clause[b]]
            secondLength = dict(letVariable)[second]
        elif second in dict(declares):
            # print("dc")
            clause[b] = [clause[b]]
            secondLength = int(dict(declares)[second])
        else:
            # print("df")
            clause[b] = [clause[b]]
            secondLength = int(dict(defines)[second])
    else:
        readAssert(second)
        if second in boolList:
            secondLength = 0
    if firstLength == -1:
        firstLength = lengthMap[repr(first)]
    if secondLength == -1:
        secondLength = lengthMap[repr(second)]
    if isinstance(clause[a], list) and clause[a][0] == 'ite':
        firstIte = True
    if isinstance(clause[b], list) and clause[b][0] == 'ite':
        secondIte = True
    return firstLength, secondLength, firstIte, secondIte


def fetchOneArgument(clause):
    arg = clause[1]
    length = -1
    argIte = False
    if not isinstance(arg, list):
        # print "variablemap ", variableMap
        # print "letvariable ", letVariable
        # print "variable ", declares
        if arg == "false" or arg == "true":
            length = 0
        elif arg in dict(declare_bools):
            length = 0
        elif arg in dict(define_bools):
            length = 0
        elif arg in variableMap:
            clause[1], length = deepcopy(variableMap.get(arg))
        elif arg in dict(letVariable):
            clause[1] = [clause[1]]
            length = int(dict(letVariable)[arg])
        elif arg in dict(declares):
            clause[1] = [clause[1]]
            length = int(dict(declares)[arg])
        else:
            clause[1] = [clause[1]]
            length = int(dict(defines)[arg])
    else:
        readAssert(arg)
        if arg in boolList:
            length = 0
    if clause[1][0] == 'ite':
        argIte = True
    if length == -1:
        length = lengthMap[repr(clause[1])]
    return length, argIte


def transferOpwithTwoItes(clause, rlist11, rlist12, rlist21, rlist22):
    condition1 = clause[1][1]
    condition2 = clause[2][1]
    del clause[:]
    clause.extend(['ite', condition1])
    itelist1 = []
    itelist1.extend(['ite', condition2])
    opList1 = []
    for element in rlist11:
        opList1.append(element)
    opList2 = []
    for element in rlist12:
        opList2.append(element)
    itelist1.extend([opList1, opList2])
    clause.append(itelist1)
    itelist2 = []
    itelist2.extend(['ite', condition2])
    opList3 = []
    for element in rlist21:
        opList3.append(element)
    opList4 = []
    for element in rlist22:
        opList4.append(element)
    itelist2.extend([opList3, opList4])
    clause.append(itelist2)


def transferOpwithOneIte(origin, clause, index, rlist1, rlist2):
    if origin:
        condition = clause[index][1]
    else:
        condition = clause[1]
    del clause[:]
    clause.append('ite')
    clause.append(condition)
    opList1 = []
    for element in rlist1:
        opList1.append(element)
    clause.append(opList1)
    opList2 = []
    for element in rlist2:
        opList2.append(element)
    clause.append(opList2)


def handleOneArgOps(origin, clause, func, ite, length, *args):
    # print(origin, clause, func, ite, length, *args, "\n")
    # if ite and length > 64:
    if ite:
        firstSolved = False
        secondSolved = False

        if origin:
            first = clause[1][2]
            second = clause[1][3]
        else:
            first = clause[2]
            second = clause[3]

        if isinstance(first, list) and first[0] == "ite":
            handleOneArgOps(False, first, func, True, length, *args)
            firstSolved = True
        if isinstance(second, list) and second[0] == "ite":
            handleOneArgOps(False, second, func, True, length, *args)
            secondSolved = True

        if not origin or not firstSolved and not secondSolved:
            # print("extract", first, *args)
            rlist1 = func(first, *args)
            rlist2 = func(second, *args)
            if repr(first) in constantValue:
                constantValue.update({repr(rlist1): int(constantValue[repr(first)])})
            if repr(second) in constantValue:
                constantValue.update({repr(rlist2): int(constantValue[repr(second)])})
            transferOpwithOneIte(origin, clause, 1, rlist1, rlist2)
            lengthMap.update({repr(clause): length})
        else:
            if firstSolved and secondSolved:
                iteList = clause[1]
                del clause[:]
                for element in iteList:
                    clause.append(element)
                lengthMap.update({repr(clause): length})
            elif firstSolved:
                rlist2 = func(clause[1][3], *args)
                clause[1][3] = rlist2
                iteList = clause[1]
                del clause[:]
                for element in iteList:
                    clause.append(element)
                lengthMap.update({repr(clause): length})
            elif secondSolved:
                rlist1 = func(clause[1][2], *args)
                clause[1][2] = rlist1
                iteList = clause[1]
                del clause[:]
                for element in iteList:
                    clause.append(element)
                lengthMap.update({repr(clause): length})
    # elif length > 64:
    else:
        # print("length > 64", clause)
        rlist = func(clause[1], *args)
        # print(clause)
        if repr(clause[1]) in constantValue:
            constantValue.update({repr(rlist): int(constantValue[repr(clause[1])])})
        lengthMap.update({repr(rlist): length})
        del clause[:]
        for element in rlist:
            clause.append(element)
            # else:
            # lengthMap.update({repr(clause): length})


def twoArgOpsHelper(clause1, clause2, func, length, *args):
    leftSolved = False
    rightSolved = False
    leftIte = isinstance(clause1[2], list) and clause1[2][0] == "ite"
    rightIte = isinstance(clause1[3], list) and clause1[3][0] == "ite"
    if leftIte:
        twoArgOpsHelper(clause1[2], clause2, func, length, *args)
        leftSolved = True
    if rightIte:
        twoArgOpsHelper(clause1[3], clause2, func, length, *args)
        rightSolved = True
    if not leftSolved and not rightSolved:
        rlist1 = func(clause1[2], clause2, *args)
        rlist2 = func(clause1[3], clause2, *args)
        clause1[2] = rlist1
        clause1[3] = rlist2
    elif leftSolved and rightSolved:
        pass
    elif leftSolved and not rightSolved:
        rlist2 = func(clause1[3], clause2, *args)
        clause1[3] = rlist2
    elif not leftSolved and rightSolved:
        rlist1 = func(clause1[2], clause2, *args)
        clause1[2] = rlist1


def handleTwoArgOps(clause, func, firstIte, secondIte, length, *args):
    # p = clause[0] == "concat"
    # if p:
    #     print(clause, firstIte, secondIte)
    # if not firstIte and not secondIte:
    #     if isinstance(clause[1], list) and len(clause[1]) > 1 and not isinstance(clause[1][0], list):
    #         clause[1] = [clause[1]]
    #     if isinstance(clause[2], list) and len(clause[2]) > 1 and not isinstance(clause[2][0], list):
    #         clause[2] = [clause[2]]
    # if firstIte and secondIte and length > 64:
    if firstIte and secondIte:
        op11 = clause[1][2]
        op12 = clause[1][3]
        op21 = clause[2][2]
        op22 = clause[2][3]
        rlist11 = func(op11, op21, *args)
        rlist12 = func(op11, op22, *args)
        rlist21 = func(op12, op21, *args)
        rlist22 = func(op12, op22, *args)
        transferOpwithTwoItes(clause, rlist11, rlist12, rlist21, rlist22)
        lengthMap.update({repr(clause): length})
    # elif firstIte and length > 64:
    elif firstIte:
        # print(clause)
        leftIte = isinstance(clause[1][2], list) and clause[1][2][0] == "ite"
        rightIte = isinstance(clause[1][3], list) and clause[1][3][0] == "ite"
        # print("leftite", leftIte, "rightite", rightIte)
        solved = False
        if not leftIte and rightIte:
            twoArgOpsHelper(clause[1][3], clause[2], func, length, *args)
            rlist = func(clause[1][2], clause[2], *args)
            clause[1][2] = rlist
            solved = True
        elif leftIte and not rightIte:
            twoArgOpsHelper(clause[1][2], clause[2], func, length, *args)
            rlist = func(clause[1][3], clause[2], *args)
            clause[1][3] = rlist
            solved = True
        elif leftIte and rightIte:
            twoArgOpsHelper(clause[1][3], clause[2], func, length, *args)
            twoArgOpsHelper(clause[1][2], clause[2], func, length, *args)
            solved = True
        else:
            # print(clause[1][2], clause[1][3], clause[2])
            rlist1 = func(clause[1][2], clause[2], *args)
            rlist2 = func(clause[1][3], clause[2], *args)
            transferOpwithOneIte(True, clause, 1, rlist1, rlist2)
            lengthMap.update({repr(clause): length})
        if solved:
            iteList = clause[1]
            del clause[:]
            for element in iteList:
                clause.append(element)
            lengthMap.update({repr(clause): length})
            # if p:
            #     print("rlist1 is", rlist1)
            #     print("rlist2 is", rlist2)
            #     print("concat after process, clause is", clause)
    # elif secondIte and length > 64:
    elif secondIte:
        leftIte = isinstance(clause[2][2], list) and clause[2][2][0] == "ite"
        rightIte = isinstance(clause[2][3], list) and clause[2][3][0] == "ite"
        solved = False
        if not leftIte and rightIte:
            twoArgOpsHelper(clause[2][3], clause[1], func, length, *args)
            rlist = func(clause[2][2], clause[1], *args)
            clause[2][2] = rlist
            solved = True
        elif leftIte and not rightIte:
            twoArgOpsHelper(clause[2][2], clause[1], func, length, *args)
            rlist = func(clause[2][3], clause[1], *args)
            clause[2][3] = rlist
            solved = True
        elif leftIte and rightIte:
            twoArgOpsHelper(clause[2][3], clause[1], func, length, *args)
            twoArgOpsHelper(clause[2][2], clause[1], func, length, *args)
            solved = True
        else:
            rlist1 = func(clause[2][2], clause[1], *args)
            rlist2 = func(clause[2][3], clause[1], *args)
            transferOpwithOneIte(True, clause, 2, rlist1, rlist2)
            lengthMap.update({repr(clause): length})
        if solved:
            iteList = clause[2]
            del clause[:]
            for element in iteList:
                clause.append(element)
            lengthMap.update({repr(clause): length})
    # elif length > 64:
    else:
        rlist = func(clause[1], clause[2], *args)
        # print("rlist", rlist)
        lengthMap.update({repr(rlist): length})
        del clause[:]
        for element in rlist:
            clause.append(element)
            # else:
            #     lengthMap.update({repr(clause): length})


def getShiftNum(bitV):
    # set default bitV has format like ['_', 'bv20', '8']
    key = bitV[:]
    # if repr(key) in variableMap:
    if isinstance(key, list):
        readAssert(key)
    # if repr(key) in variableMap:
    #     key = variableMap[key][0]
    elif key in variableMap:
        key = variableMap[key][0]
    # print('key is ', key)
    # print(variableMap)
    # print(constantValue)
    # print(key)
    if repr(key) in constantValue:
        return constantValue[repr(key)]
    elif isinstance(key, list) and key[0] == '_' and key[1].startswith('bv'):
        return int(key[1].replace('bv', ''))
    else:
        print("shift num is not const, can't solve", key)
        sys.exit()


def readAssert(clause):
    # print("read assert", clause)
    if clause[0] == 'let':
        removeLet = checkLetClaim(clause[1])
        print("check let done")
        # length, transform = readAssert(clause[2])
        readAssert(clause[2])
        if removeLet:
            clause.pop(0)
            clause.pop(0)
            # return length, length > 64
    elif clause[0] == 'and':
        trans = False
        length = 0
        # print(clause)
        for i in range(1, len(clause)):
            # print(clause[i])
            # length, transform = readAssert(clause[i])
            readAssert(clause[i])
            if length > 64:
                trans = True
        boolList.append(clause)
        # return length, trans
    elif clause[0] == "not":
        # length, transform = readAssert(clause[1])
        readAssert(clause[1])
        boolList.append(clause)
        # return length, length > 64
    elif clause[0] == "or":
        trans = False
        length = 0
        for i in range(1, len(clause)):
            # length, transform = readAssert(clause[i])
            readAssert(clause[i])
            if length > 64:
                trans = True
        boolList.append(clause)
        # return length, trans
    elif clause[0] == "=":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        # deal with = with ite
        if firstLength > 64:
            reconstructEqual(clause, firstLength, firstIte, secondIte)
        lengthMap.update({repr(clause): firstLength})
        return firstLength, firstLength > 64
    elif clause[0] == "distinct":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        if firstLength > 64:
            copyClause = deepcopy(clause)
            copyClause[0] = "="
            reconstructEqual(copyClause, firstLength, firstIte, secondIte)
            del clause[:]
            clause.append("not")
            clause.append(copyClause)
    elif clause[0] == "bitsumhelper":
        length, ite = fetchOneArgument(clause)
        resultList = [['bvand', clause[1], ['bvsub', clause[1], '_ bv1 9']]]
        lengthMap.update({repr(resultList): length})
        del clause[:]
        for element in resultList:
            clause.append(element)
    elif clause[0] == "bvcomp":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        if firstLength > 64:
            equalAssert = ['=', clause[1], clause[2]]
            readAssert(equalAssert)
            compList = ['ite', equalAssert, '_ bv1 1', '_ bv1 0']
            del clause[:]
            for element in compList:
                clause.append(element)
            return 1, True
        return 1, False
    elif isinstance(clause[0], list) and clause[0][1] == "extract":
        # print("extract1", clause)
        length, ite = fetchOneArgument(clause)
        # print("extract2", clause, length, ite)
        extractLength = int(clause[0][2]) - int(clause[0][3]) + 1
        handleOneArgOps(True, clause, getExtractResultList, ite, extractLength,
                        int(clause[0][2]), int(clause[0][3]), length)
        # print("extract 3", clause)
        return extractLength, length > 64
    elif clause[0] == "concat":
        # print("concat 1", clause)
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        # print("concat 2", clause)
        handleTwoArgOps(clause, getConcatResultList, firstIte, secondIte, firstLength + secondLength,
                        firstLength, secondLength)
        # print("concat 3", clause)
        return firstLength + secondLength, firstLength > 64 or secondLength > 64
    elif clause[0] == "bvadd":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        # print("after fetch, bvadd is ", clause)
        # print(firstIte, secondIte)
        handleTwoArgOps(clause, getAddResultList, firstIte, secondIte, firstLength, firstLength, 0, [], 1)
        return firstLength, firstLength > 64
    elif clause[0] == "bvsub":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        handleTwoArgOps(clause, getSubResultList, firstIte, secondIte, firstLength, firstLength, 0, [], 1)
        return firstLength, firstLength > 64
    elif clause[0] == "bvmul":
        # print("bvmul", clause)
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        # print("bvmul now", clause)
        handleTwoArgOps(clause, getMulResultList, firstIte, secondIte, firstLength, firstLength)
        # print("bvmul final", clause)
        return firstLength, firstLength > 64
    elif clause[0] == "bvneg":
        length, ite = fetchOneArgument(clause)
        handleOneArgOps(True, clause, getNegResultList, ite, length, length, '_ bv1 64', [], 1)
        return length, length > 64
    elif clause[0] == "bvnot":
        length, ite = fetchOneArgument(clause)
        handleOneArgOps(True, clause, getNotResultList, ite, length, length)
        return length, length > 64
    elif clause[0] == "bvand":
        # b = clause[:]
        # print(b)
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        handleTwoArgOps(clause, getAndResultList, firstIte, secondIte, firstLength, firstLength)
        # print(b, clause)
        return firstLength, firstLength > 64
    elif clause[0] == "bvor":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        handleTwoArgOps(clause, getOrResultList, firstIte, secondIte, firstLength, firstLength)
        return firstLength, firstLength > 64
    elif clause[0] == "bvnand":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        handleTwoArgOps(clause, getNandResultList, firstIte, secondIte, firstLength, firstLength)
        return firstLength, firstLength > 64
    elif clause[0] == "bvnor":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        handleTwoArgOps(clause, getNorResultList, firstIte, secondIte, firstLength, firstLength)
        return firstLength, firstLength > 64
    elif clause[0] == "bvxor":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        handleTwoArgOps(clause, getXorResultList, firstIte, secondIte, firstLength, firstLength)
        return firstLength, firstLength > 64
    elif clause[0] == "bvxnor":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        handleTwoArgOps(clause, getXnorResultList, firstIte, secondIte, firstLength, firstLength)
        return firstLength, firstLength > 64
    elif clause[0] == "bvult":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        handleTwoArgOps(clause, getUltResultList, firstIte, secondIte, firstLength, firstLength, [])
        return firstLength, firstLength > 64
    elif clause[0] == "bvule":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        handleTwoArgOps(clause, getUleResultList, firstIte, secondIte, firstLength, firstLength, [])
        return firstLength, firstLength > 64
    elif clause[0] == "bvugt":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        handleTwoArgOps(clause, getUgtResultList, firstIte, secondIte, firstLength, firstLength, [])
        return firstLength, firstLength > 64
    elif clause[0] == "bvuge":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        handleTwoArgOps(clause, getUgeResultList, firstIte, secondIte, firstLength, firstLength, [])
        return firstLength, firstLength > 64
    elif clause[0] == "bvslt":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        handleTwoArgOps(clause, getSltResultList, firstIte, secondIte, firstLength, firstLength, [])
        return firstLength, firstLength > 64
    elif clause[0] == "bvsle":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        handleTwoArgOps(clause, getSleResultList, firstIte, secondIte, firstLength, firstLength, [])
        return firstLength, firstLength > 64
    elif clause[0] == "bvsgt":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        handleTwoArgOps(clause, getSgtResultList, firstIte, secondIte, firstLength, firstLength, [])
        return firstLength, firstLength > 64
    elif clause[0] == "bvsge":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        handleTwoArgOps(clause, getSgeResultList, firstIte, secondIte, firstLength, firstLength, [])
        return firstLength, firstLength > 64
    elif clause[0] == "bvshl":
        # print("bvshl1", clause)
        length, ite = fetchOneArgument(clause)
        # print("bvshl2", clause)
        shiftNum = getShiftNum(clause[2])
        # print("shift number is", shiftNum)
        handleOneArgOps(True, clause, getShlResultList, ite, length, shiftNum, length)
        # print("bvshl3", clause)
        return length, length > 64
    elif clause[0] == "bvlshr":
        # print("bvlshr1", clause)
        length, ite = fetchOneArgument(clause)
        # print("bvlshr2", clause)
        shiftNum = getShiftNum(clause[2])
        # print("shift num is", shiftNum)
        handleOneArgOps(True, clause, getLshrResultList, ite, length, shiftNum, length)
        # print("bvlshr3", clause)
        return length, length > 64
    elif clause[0] == "bvashr":
        length, ite = fetchOneArgument(clause)
        shiftNum = getShiftNum(clause[2])
        handleOneArgOps(True, clause, getAshrResultList, ite, length, shiftNum, length)
        return length, length > 64
    elif isinstance(clause[0], list) and clause[0][1] == "rotate_left":
        length, ite = fetchOneArgument(clause)
        handleOneArgOps(True, clause, getRotateLResultList, ite, length, int(clause[0][2]), length)
        return length, length > 64
    elif isinstance(clause[0], list) and clause[0][1] == "rotate_right":
        length, ite = fetchOneArgument(clause)
        handleOneArgOps(True, clause, getRotateRResultList, ite, length, int(clause[0][2]), length)
        return length, length > 64
    elif isinstance(clause[0], list) and clause[0][1] == "repeat":
        length, ite = fetchOneArgument(clause)
        repeatLength = length * int(clause[0][2])
        handleOneArgOps(True, clause, getRepeatResultList, ite, repeatLength, length, int(clause[0][2]))
        return repeatLength, repeatLength > 64
    elif isinstance(clause[0], list) and clause[0][1] == "zero_extend":
        # print("zero_extend", clause)
        length, ite = fetchOneArgument(clause)
        # print("zero_extend now", clause)
        extendLength = length + int(clause[0][2])
        handleOneArgOps(True, clause, getZeroExtendResultList, ite, extendLength, length, int(clause[0][2]))
        # print("zero_extend final", clause)
        return extendLength, extendLength > 64
    elif isinstance(clause[0], list) and clause[0][1] == "sign_extend":
        length, ite = fetchOneArgument(clause)
        extendLength = length + int(clause[0][2])
        handleOneArgOps(True, clause, getSignExtendResultList, ite, extendLength, length, int(clause[0][2]))
        return extendLength, extendLength > 64
    elif clause[0] == "_" and clause[1].startswith('bv'):
        length = int(clause[2])
        value = int(clause[1].replace('bv', ''))
        if length > 64:
            handleConstant(clause)
        # else:
        #     tmp = clause[:]
        #     del clause[:]
        #     clause.append(tmp)
        else:
            origin = ' '.join(clause)
            del clause[:]
            clause.append(origin)
        lengthMap.update({repr(clause): length})
        constantValue.update({repr(clause): int(value)})
        return length, length > 64
    elif clause[0] == "ite":
        fetchOneArgument(clause)
        length1, length2, firstIte, secondIte = fetchTwoArguments(clause, 2, 3)
        lengthMap.update({repr(clause): length1})
        return length1, length1 > 64
    elif clause[0] == "bvudiv" or clause[0] == "bvurem" or clause[0] == "bvsdiv" or clause[0] == "bvsrem" or clause[0] \
            == "bvsmod":
        firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(clause, 1, 2)
        if firstLength > 64:
            print("div or mod, can't solve")
            sys.exit()
        return firstLength, firstLength > 64
    elif clause == "true":
        pass
    elif clause == "false":
        pass
    elif isinstance(clause, str) and (clause == "fasle" or clause == "true"):
        pass
    elif isinstance(clause, str) and clause in dict(declare_bools):
        return 0, False
    elif isinstance(clause, str) and clause in dict(define_bools):
        return 0, False
    elif isinstance(clause, str) and inListofTuple(boolList, clause):
        return 0, False
    # elif isinstance(clause, str) and clause in dict(boolList):
    #     return 0, False
    elif isinstance(clause, str) and clause in variableMap:
        clause, length = deepcopy(variableMap.get(clause))
        return length, True
    elif isinstance(clause, str) and clause in dict(letVariable):
        # clause[1] = [clause[1]]
        length = int(dict(letVariable)[clause])
        return length, True
    elif isinstance(clause, str) and clause in dict(declares):
        # clause[1] = [clause[1]]
        length = int(dict(declares)[clause])
        return length, False
    elif isinstance(clause, str):
        # clause[1] = [clause[1]]
        length = int(dict(defines)[clause])
        return length, False

        # elif not isinstance(clause, list):
        #     if clause in variableMap:
        #         c, length = variableMap[clause]
        #         del clause[:]
        #         for element in c:
        #             clause.append(element)
        #         return length
        #     elif clause in dict(declares):
        #         return int(dict(declares)[clause])


def inListofTuple(listTuples, str):
    for l in listTuples:
        if l[0] == str:
            return True
    return False


def handleConstant(clause):
    length = int(clause[2])
    l = int(math.ceil(float(length) / 64))
    value = int(clause[1].replace('bv', ''))
    if value < int(math.pow(2, 64)):
        # print("const", value, "< 2^64")
        del clause[:]
        for i in range(0, l):
            if i == 0:
                if length % 64 == 0:
                    clause.append('_ bv0 64')
                else:
                    clause.append('_ bv0 ' + str(length % 64))
            elif i < l - 1:
                clause.append('_ bv0 64')
            else:
                clause.append('_ bv' + str(value) + ' 64')
    else:
        del clause[:]
        # print("const", value, "> 2^64")
        segNum = int(math.floor(value ** (1 / math.pow(2, 64))))
        # print("segNum", segNum, "l", l)
        for i in range(0, l - segNum):
            clause.append('_ bv0 64')
        while segNum > 0:
            clause.append('_ bv' + str(value // 2 ** (64 * segNum)) + ' 64')
            value = value % 2 ** (64 * segNum)
            segNum -= 1


def transferEqual(c1, c2, length):
    equalList = []
    l = int(math.ceil(float(length) / 64))
    if l == 1:
        equalList.append(opEqual(c1[0], c2[0]))
    else:
        for i in range(0, l):
            equalList.append(opEqual(c1[i], c2[i]))
    return equalList, l


def reconstructEqual(clause, length, firstIte, secondIte):
    if clause[1] in boolList and clause[2] in boolList:
        pass
    # todo: what if ite in ite
    elif firstIte and secondIte:
        op11 = clause[1][2]
        op12 = clause[1][3]
        op21 = clause[2][2]
        op22 = clause[2][3]
        equalList11, l11 = transferEqual(op11, op21, length)
        equalList12, l12 = transferEqual(op11, op22, length)
        equalList21, l21 = transferEqual(op12, op21, length)
        equalList22, l22 = transferEqual(op12, op22, length)
        transferOpwithTwoItes(clause, equalList11, equalList12, equalList21, equalList22)
        # lengthMap.update({repr(clause): length})
    elif firstIte:
        op1 = clause[1][2]
        op2 = clause[1][3]
        equalList1, l1 = transferEqual(op1, clause[2], length)
        equalList2, l2 = transferEqual(op2, clause[2], length)
        transferOpwithOneIte(True, clause, 1, equalList1, equalList2)
        # lengthMap.update({repr(clause): length})
    elif secondIte:
        op1 = clause[2][2]
        op2 = clause[2][3]
        equalList1, l1 = transferEqual(clause[1], op1, length)
        equalList2, l2 = transferEqual(clause[1], op2, length)
        transferOpwithOneIte(True, clause, 2, equalList1, equalList2)
        # lengthMap.update({repr(clause): length})
    else:
        equalList, l = transferEqual(clause[1], clause[2], length)
        del clause[:]
        for el in opAnd(equalList, l):
            clause.append(el)
            # lengthMap.update({repr(clause): length})
    boolList.append(clause)


def checkLetClaim(claims):
    removeList = []
    # print("read claim", claims)
    for claim in claims:
        variable = claim[0]
        # if variable == "?v_44":
        #     global printAllowed
        #     printAllowed = True
        exp = claim[1]
        print(variable)
        # print(len(lengthMap), len(variableMap), len(constantValue), len(letVariable))
        # print(constantValue)
        # print("?v_6 is", variableMap.get("?v_6"))
        # if variable == "?v_69":
        # print("?v_68 is", variableMap.get("?v_68"))
        # print("?v_5 is", variableMap.get("?v_5"))
        # if variable == "?v_3":
        #     print("?v_3 origin", exp)
        if exp[0] == "=":
            # print variable, exp
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                reconstructEqual(exp, firstLength, firstIte, secondIte)
            variableMap.update({variable: (exp, firstLength)})
            # print 'let = ', exp
        elif exp[0] == "not":
            readAssert(exp[1])
            boolList.append((variable, exp))
        elif exp[0] == "and":
            for i in range(1, len(exp)):
                readAssert(exp[i])
            boolList.append((variable, exp))
        elif exp[0] == "distinct":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                copyClause = deepcopy(exp)
                copyClause[0] = "="
                reconstructEqual(copyClause, firstLength, firstIte, secondIte)
                del exp[:]
                exp.append("not")
                exp.append(copyClause)
                boolList.append((variable, exp))
        elif isinstance(exp[0], list) and exp[0][1] == 'extract':
            # if variable == "?v_0":
            #     print("?v_0", exp)
            length, ite = fetchOneArgument(exp)
            # if variable == "?v_0":
            #     print("?v_0", exp, ite, length)
            if length > 64:
                extractLength = int(exp[0][2]) - int(exp[0][3]) + 1
                handleOneArgOps(True, exp, getExtractResultList, ite, extractLength,
                                int(exp[0][2]), int(exp[0][3]), length)
                # extractResultList = getExtractResultList(exp[1], int(extract[2]), int(extract[3]),
                #                                          length)
                # if variable == "?v_0":
                #     print("?v_0", exp)
                variableMap.update({variable: (exp, extractLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, length))
        elif exp[0] == "concat":
            # print("concat 1", exp)
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            # print("concat 2", exp, firstIte, secondIte)
            if firstLength + secondLength > 64:
                # concatResultList = getConcatResultList(exp[1], exp[2], firstLength, secondLength)
                handleTwoArgOps(exp, getConcatResultList, firstIte, secondIte, firstLength + secondLength,
                                firstLength, secondLength)
                # print("concat 3", exp)
                variableMap.update({variable: (exp, firstLength + secondLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvadd":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # addResultList = getAddResultList(exp[1], exp[2], firstLength, 0, [])
                handleTwoArgOps(exp, getAddResultList, firstIte, secondIte, firstLength, firstLength, 0, [], 1)
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvsub":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # subResultList = getSubResultList(exp[1], exp[2], firstLength, 0, [])
                handleTwoArgOps(exp, getSubResultList, firstIte, secondIte, firstLength, firstLength, 0, [], 1)
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvmul":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # mulResultList = getMulResultList(exp[1], exp[2], firstLength)
                handleTwoArgOps(exp, getMulResultList, firstIte, secondIte, firstLength, firstLength)
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvneg":
            length, ite = fetchOneArgument(exp)
            if length > 64:
                # negResultList = getNegResultList(exp[1], length, '_ bv1 64', [])
                handleOneArgOps(True, exp, getNegResultList, ite, length, length, '_ bv1 64', [], 1)
                variableMap.update({variable: (exp, length)})
                removeList.append(claim)
            else:
                letVariable.append((variable, length))
        elif exp[0] == "bvnot":
            length, ite = fetchOneArgument(exp)
            if length > 64:
                # notResultList = getNotResultList(exp[1], length)
                handleOneArgOps(True, exp, getNotResultList, ite, length, length)
                variableMap.update({variable: (exp, length)})
                removeList.append(claim)
            else:
                letVariable.append((variable, length))
        elif exp[0] == "bvand":
            # if variable == "?v_69":
            #     print(exp)
            # if variable == "?v_69":
            #     print("?v_69", exp)
            #     print("?v_6", variableMap.get("?v_6"))
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            # if variable == "?v_69":
            #     print(exp, firstLength, secondLength, firstIte, secondIte)
            if firstLength > 64:
                # andResultList = getAndResultList(exp[1], exp[2], firstLength)
                handleTwoArgOps(exp, getAndResultList, firstIte, secondIte, firstLength, firstLength)
                # if variable == "?v_6":
                #     print(exp)
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
                # if variable == "?v_69":
                #     print("?v_69", exp)
                #     print("?v_69", variableMap.get("?v_69"))
        elif exp[0] == "bvor":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # orResultList = getOrResultList(exp[1], exp[2], firstLength)
                handleTwoArgOps(exp, getOrResultList, firstIte, secondIte, firstLength, firstLength)
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
            if variable == "?v_1":
                print("?v_1", exp)
        elif exp[0] == "bvnand":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # nandResultList = getNandResultList(exp[1], exp[2], firstLength)
                handleTwoArgOps(exp, getNandResultList, firstIte, secondIte, firstLength, firstLength)
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvnor":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # norResultList = getNorResultList(exp[1], exp[2], firstLength)
                handleTwoArgOps(exp, getNorResultList, firstIte, secondIte, firstLength, firstLength)
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvxor":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # xorResultList = getXorResultList(exp[1], exp[2], firstLength)
                handleTwoArgOps(exp, getXorResultList, firstIte, secondIte, firstLength, firstLength)
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvxnor":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # xnorResultList = getXnorResultList(exp[1], exp[2], firstLength)
                handleTwoArgOps(exp, getXnorResultList, firstIte, secondIte, firstLength, firstLength)
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvult":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # ultResultList = getUltResultList(exp[1], exp[2], firstLength, [])
                handleTwoArgOps(exp, getUltResultList, firstIte, secondIte, firstLength, firstLength, [])
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvule":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # uleResultList = getUleResultList(exp[1], exp[2], firstLength, [])
                handleTwoArgOps(exp, getUleResultList, firstIte, secondIte, firstLength, firstLength, [])
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvugt":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # ugtResultList = getUgtResultList(exp[1], exp[2], firstLength, [])
                handleTwoArgOps(exp, getUgtResultList, firstIte, secondIte, firstLength, firstLength, [])
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvuge":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # ugeResultList = getUgeResultList(exp[1], exp[2], firstLength, [])
                handleTwoArgOps(exp, getUgeResultList, firstIte, secondIte, firstLength, firstLength, [])
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvslt":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # sltResultList = getSltResultList(exp[1], exp[2], firstLength, [])
                handleTwoArgOps(exp, getSltResultList, firstIte, secondIte, firstLength, firstLength, [])
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvsle":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # sleResultList = getSleResultList(exp[1], exp[2], firstLength, [])
                handleTwoArgOps(exp, getSleResultList, firstIte, secondIte, firstLength, firstLength, [])
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvsgt":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # sgtResultList = getSgtResultList(exp[1], exp[2], firstLength, [])
                handleTwoArgOps(exp, getSgtResultList, firstIte, secondIte, firstLength, firstLength, [])
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvsge":
            firstLength, secondLength, firstIte, secondIte = fetchTwoArguments(exp, 1, 2)
            if firstLength > 64:
                # sgeResultList = getSgeResultList(exp[1], exp[2], firstLength, [])
                handleTwoArgOps(exp, getSgeResultList, firstIte, secondIte, firstLength, firstLength, [])
                variableMap.update({variable: (exp, firstLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, firstLength))
        elif exp[0] == "bvshl":
            length, ite = fetchOneArgument(exp)
            shiftNum = getShiftNum(exp[2])
            if length > 64:
                # shlResultList = getShlResultList(exp[1], shiftNum, length)
                handleOneArgOps(True, exp, getShlResultList, ite, length, shiftNum, length)
                variableMap.update({variable: (exp, length)})
                removeList.append(claim)
            else:
                letVariable.append((variable, length))
        elif exp[0] == "bvlshr":
            length, ite = fetchOneArgument(exp)
            shiftNum = getShiftNum(exp[2])
            if length > 64:
                # lshrResultList = getLshrResultList(exp[1], shiftNum, length)
                handleOneArgOps(True, exp, getLshrResultList, ite, length, shiftNum, length)
                variableMap.update({variable: (exp, length)})
                removeList.append(claim)
            else:
                letVariable.append((variable, length))
        elif exp[0] == "bvashr":
            length, ite = fetchOneArgument(exp)
            shiftNum = getShiftNum(exp[2])
            if length > 64:
                # ashrResultList = getAshrResultList(exp[1], shiftNum, length)
                handleOneArgOps(True, exp, getAshrResultList, ite, length, shiftNum, length)
                variableMap.update({variable: (exp, length)})
                removeList.append(claim)
            else:
                letVariable.append((variable, length))
        elif isinstance(exp[0], list) and exp[0][1] == "rotate_left":
            length, ite = fetchOneArgument(exp)
            if length > 64:
                rotateLResultList = getRotateLResultList(exp[1], int(exp[0][2]), length)
                variableMap.update({variable: (rotateLResultList, length)})
                removeList.append(claim)
            else:
                letVariable.append((variable, length))
        elif isinstance(exp[0], list) and exp[0][1] == "rotate_right":
            length, ite = fetchOneArgument(exp)
            if length > 64:
                # rotateRResultList = getRotateRResultList(exp[1], int(exp[0][2]), length)
                handleOneArgOps(True, exp, getRotateLResultList, ite, length, int(exp[0][2]), length)
                variableMap.update({variable: (exp, length)})
                removeList.append(claim)
            else:
                letVariable.append((variable, length))
        elif isinstance(exp[0], list) and exp[0][1] == "repeat":
            length, ite = fetchOneArgument(exp)
            repeatLength = length * int(exp[0][2])
            if repeatLength > 64:
                # repeatResultList = getRepeatResultList(exp[1], length, int(exp[0][2]))
                handleOneArgOps(True, exp, getRepeatResultList, ite, repeatLength, length, int(exp[0][2]))
                variableMap.update({variable: (exp, length * int(exp[0][2]))})
                removeList.append(claim)
            else:
                letVariable.append((variable, length))
        elif isinstance(exp[0], list) and exp[0][1] == "zero_extend":
            length, ite = fetchOneArgument(exp)
            extendLength = length + int(exp[0][2])
            maybeConst = exp[1]
            maybeExp = exp[:]
            if extendLength > 64:
                # zeroExtendResultList = getZeroExtendResultList(exp[1], length, int(exp[0][2]))
                handleOneArgOps(True, exp, getZeroExtendResultList, ite, extendLength, length, int(exp[0][2]))
                variableMap.update({variable: (exp, extendLength)})
                if repr(maybeConst) in constantValue:
                    constantValue.update({repr(maybeExp): int(constantValue[repr(maybeConst)])})
                removeList.append(claim)
            else:
                letVariable.append((variable, length))
        elif isinstance(exp[0], list) and exp[0][1] == "sign_extend":
            length, ite = fetchOneArgument(exp)
            extendLength = length + int(exp[0][2])
            if extendLength > 64:
                # signExtendResultList = getSignExtendResultList(exp[1], length, int(exp[0][2]))
                handleOneArgOps(True, exp, getSignExtendResultList, ite, extendLength, length, int(exp[0][2]))
                variableMap.update({variable: (exp, extendLength)})
                removeList.append(claim)
            else:
                letVariable.append((variable, length))
        elif exp[0] == "_" and exp[1].startswith('bv'):
            length = int(exp[2])
            value = int(exp[1].replace('bv', ''))
            if length > 64:
                handleConstant(exp)
                variableMap.update({variable: (exp, length)})
                removeList.append(claim)
            else:
                letVariable.append((variable, length))
            constantValue.update({repr(exp): int(value)})
        elif exp[0] == "ite":
            # print("ite is ", exp)
            readAssert(exp[1])
            length = None
            if not isinstance(exp[2], list):
                if exp[2] in variableMap:
                    exp[2], length = variableMap[exp[2]]
                elif exp[2] in dict(declares):
                    length = int(dict(declares)[exp[2]])
                elif exp[2] in dict(defines):
                    length = int(dict(defines)[exp[2]][1])
            else:
                length, transform = readAssert(exp[2])
            if not isinstance(exp[3], list):
                if exp[3] in variableMap:
                    exp[3], length = variableMap[exp[3]]
                elif exp[3] in dict(declares):
                    length = int(dict(declares)[exp[3]])
                elif exp[3] in dict(defines):
                    length = int(dict(defines)[exp[3]][1])
            else:
                # print("exp3 is ", exp[3])
                length, transform = readAssert(exp[3])
            variableMap.update({variable: (exp, length)})
        elif not isinstance(exp, list):
            # print 'let constant ', exp
            # assume it is in format ['_', 'bv20', '12']
            length = exp[2]
            handleConstant(exp)
            letVariable.append((variable, length))
    for remove in removeList:
        claims.remove(remove)
    if len(claims) == 0:
        return True
    return False


def getBeforeClauses(clauseList):
    beforeVariableList = []
    for c in clauseList:
        if checkType(c) != "declare":
            beforeVariableList.append(c)
        else:
            break
    return beforeVariableList


def getAfterClauses(clauseList):
    afterAssertList = []
    for c in clauseList:
        if checkType(c) == "check" or checkType(c) == "exit":
            afterAssertList.append(c)
    return afterAssertList


def assertListtoString(assertList):
    lstring = repr(assertList)
    lstring = lstring.replace("[", "(")
    lstring = lstring.replace("]", ")")
    lstring = lstring.replace("\'", '')
    lstring = lstring.replace(",", '')
    return lstring


def writeFile(fileName):
    smtFileOutput = open(fileName, 'w')
    beforeClauses = getBeforeClauses(clauseList)
    afterClauses = getAfterClauses(clauseList)
    for bc in beforeClauses:
        smtFileOutput.write(bc + '\n')
    for dc in declares:
        declare = "(declare-fun " + dc[0] + " () (_ BitVec " + str(dc[1]) + "))\n"
        smtFileOutput.write(declare)
    for dc_bl in declare_bools:
        declare = "(declare-fun " + dc_bl[0] + " () Bool)\n"
        smtFileOutput.write(declare)
    for df in defines:
        declare = "(define-fun " + df[0] + " () (_ BitVec " + str(df[1][1]) + ") (" + df[1][0] + "))\n"
        smtFileOutput.write(declare)
    for df_bl in define_bools:
        declare = "(define-fun " + df_bl[0] + " () Bool (" + df_bl[1] + "))\n"
        smtFileOutput.write(declare)
    for al in assertList:
        smtFileOutput.write("(assert " + assertListtoString(al) + ")\n")
    for ac in afterClauses:
        smtFileOutput.write(ac + '\n')
    smtFileOutput.close()


def process():
    for al in assertList:
        readAssert(al)


with open(sys.argv[1]) as f:
    smtFile = f.read()

tryRunWobit()
sys.setrecursionlimit(1000000)
clauseList = readClauses(smtFile)
getDeclareFun(clauseList)
mapVariables()
getDefineFun(clauseList)
assertList = getAsserts(clauseList)
process()
fileName = str(sys.argv[1]).split('/')[-1:][0]
fileName = 'tmp/' + fileName[:len(fileName) - 5] + '_new.smt2'
if not os.path.exists('tmp/'):
    os.makedirs('tmp/')
writeFile(fileName)
print("start")
p = Popen(["stp_simple", fileName], stdout=PIPE)
output, err = p.communicate()
print(output.rsplit(b"\n")[0].decode("utf-8"))
# todo: handle nested ite in operation with left and right branches
# todo: handle bvshl, bvlshr, bvashr, variable as shift number
# todo: RecursionError: maximum recursion depth exceeded while getting the repr of an object

# issue: CreateBVConst: numeric overflow errorFatal Error: this is because the generated smt-lib file is too large,
# let variable with bit-vec longer than 64-bit can only be extracted and put back into the assert, make every
# occurance of this variable be replaced by a very long exp

# issue: bvshl, bvlshr, bvashr, the shift number can be a variable, but it is not possible to transform with a variable
# what i did in transformation is get the exact shift number as interger and extract anc concat corresponding segments
