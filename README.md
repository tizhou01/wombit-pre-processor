## Prerequisite
Please put your ./stp_simple executable in the root directory or add your ./stp_simple in your env path.

### To run:

`python3 execute.py ${path to smt2 file}`

This will try to run wombit on the smt2 file first. If wombit can solve it, return the result from wombit, otherwise, it
will preprocess the smt2 file and save it to /tmp folder and then run wombit on it. 