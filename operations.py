import math


def opAnd(clauseList, length):
    andList = ['and']
    for i in range(0, length):
        andList.append(clauseList[i])
    return andList


def opOr(clauseList, length):
    orList = ['or']
    for i in range(0, length):
        orList.append(clauseList[i])
    return orList


def opRotateL(left, exp):
    return [['_ rotate_left ' + str(left)], exp]


def opRotateR(right, exp):
    return [['_ rotate_right ' + str(right)], exp]


def opConcat(left, right):
    return ['concat', left, right]


def opExtract(left, right, exp):
    return [['_ extract ' + str(left) + ' ' + str(right)], exp]


def opZeroExtend(num, exp):
    return [['_ zero_extend ' + str(num)], exp]


def opRepeat(exp, times):
    return [['_ repeat ' + str(times)], exp]


def opEqual(left, right):
    return ['=', left, right]


def opBVNOT(exp):
    return ['bvnot', exp]


def opBVAND(exp1, exp2):
    return ['bvand', exp1, exp2]


def opBVOR(exp1, exp2):
    return ['bvor', exp1, exp2]


def opBVNEG(exp):
    return ['bvneg', exp]


def opBVADD(exp1, exp2):
    return ['bvadd', exp1, exp2]


def opBVMUL(exp1, exp2):
    return ['bvmul', exp1, exp2]


def opBVSHL(exp1, exp2):
    return ['bvshl', exp1, exp2]


def opBVASHR(exp1, exp2):
    return ['bvashr', exp1, exp2]


def opBVLSHR(exp1, exp2):
    return ['bvlshr', exp1, exp2]


def opBVULT(exp1, exp2):
    return ['bvult', exp1, exp2]


def opBVNAND(exp1, exp2):
    return ['bvnand', exp1, exp2]


def opBVNOR(exp1, exp2):
    return ['bvnor', exp1, exp2]


def opBVXOR(exp1, exp2):
    return ['bvxor', exp1, exp2]


def opBVXNOR(exp1, exp2):
    return ['bvxnor', exp1, exp2]


def opBVSUB(exp1, exp2):
    return ['bvsub', exp1, exp2]


def opBASHR(exp1, exp2):
    return ['bvashr', exp1, exp2]


def opBVULE(exp1, exp2):
    return ['bvule', exp1, exp2]


def opBVUGT(exp1, exp2):
    return ['bvugt', exp1, exp2]


def opBVUGE(exp1, exp2):
    return ['bvuge', exp1, exp2]


def opBVSLT(exp1, exp2):
    return ['bvslt', exp1, exp2]


def opBVSLE(exp1, exp2):
    return ['bvsle', exp1, exp2]


def opBVSGT(exp1, exp2):
    return ['bvsgt', exp1, exp2]


def opBVSGE(exp1, exp2):
    return ['bvsge', exp1, exp2]


def carryoutBVNEG(exp, c, length):
    return opZeroExtend(length - 1, opExtract(1, 1, opBVADD(
        opExtract(63, 63, opBVADD(opZeroExtend(1, opExtract(62, 0, opBVNOT(exp))), c)),
        opZeroExtend(1, opExtract(63, 63, opBVNOT(exp))))))


def carryoutBVADD(exp1, exp2, c, length):
    if c == 0:
        return opZeroExtend(length - 1, opExtract(1, 1, opBVADD(
            opBVADD(opZeroExtend(1, opExtract(63, 63, exp1)), opZeroExtend(1, opExtract(63, 63, exp2))),
            opZeroExtend(1, opExtract(63, 63, opBVADD(opZeroExtend(1, opExtract(62, 0, exp1)),
                                                      opZeroExtend(1, opExtract(62, 0, exp2))))))))
    else:
        return opZeroExtend(length - 1, opExtract(1, 1, opBVADD(
            opBVADD(opZeroExtend(1, opExtract(63, 63, exp1)), opZeroExtend(1, opExtract(63, 63, exp2))),
            opZeroExtend(1, opExtract(63, 63, opBVADD(opBVADD(opZeroExtend(1, opExtract(62, 0, exp1)),
                                                              opZeroExtend(1, opExtract(62, 0, exp2))), c))))))


def carryoutBVMUL(exp1, exp2):
    return opBVADD(
        opBVADD(opBVLSHR(opBVMUL(opZeroExtend(32, opExtract(63, 32, exp1)), opZeroExtend(32, opExtract(31, 0, exp2))),
                         ['_ bv32 64']),
                opBVLSHR(opBVMUL(opZeroExtend(32, opExtract(63, 32, exp2)), opZeroExtend(32, opExtract(31, 0, exp1))),
                         ['_ bv32 64'])),
        opBVADD(opBVMUL(opZeroExtend(32, opExtract(63, 32, exp2)), opZeroExtend(32, opExtract(63, 32, exp1))),
                opZeroExtend(63, opExtract(1, 1, opBVADD(
                    opBVADD(opZeroExtend(1, opExtract(63, 63, opBVMUL(opZeroExtend(32, opExtract(31, 0, exp1)),
                                                                      opZeroExtend(32, opExtract(31, 0, exp2))))),
                            opZeroExtend(1, opExtract(31, 31, opBVMUL(opZeroExtend(32, opExtract(63, 32, exp1)),
                                                                      opZeroExtend(32, opExtract(31, 0, exp2)))))),
                    opZeroExtend(1, opExtract(31, 31, opBVMUL(opZeroExtend(32, opExtract(31, 0, exp1)),
                                                              opZeroExtend(32, opExtract(63, 32, exp2))))))))))


def carryoutBVSUB(exp1, exp2, c, length):
    if c == 0:
        return opZeroExtend(length - 1, opExtract(1, 1, opBVADD(
            opBVADD(opZeroExtend(1, opExtract(63, 63, exp1)), opZeroExtend(1, opExtract(63, 63, opBVNEG(exp2)))),
            opZeroExtend(1, opExtract(63, 63, opBVADD(opZeroExtend(1, opExtract(62, 0, exp1)),
                                                      opZeroExtend(1, opExtract(62, 0, opBVNEG(exp2)))))))))
    else:
        return opZeroExtend(length - 1, opExtract(1, 1, opBVADD(
            opBVADD(opZeroExtend(1, opExtract(63, 63, exp1)), opZeroExtend(1, opExtract(63, 63, opBVNEG(exp2)))),
            opZeroExtend(1, opExtract(63, 63, opBVADD(opBVADD(opZeroExtend(1, opExtract(62, 0, exp1)),
                                                              opZeroExtend(1, opExtract(62, 0, opBVNEG(exp2)))), c))))))


# lower bit has lower index
def getAddResultList(exp1, exp2, length, c, result, i):
    # print exp1, exp2
    l = int(math.ceil(float(length) / 64))
    mod = length % 64
    if i == 1:
        element = opBVADD(exp1[l - i], exp2[l - i])
        result.append(element)
        # if length > 128:
        #     return getAddResultList(exp1[:-1], exp2[:-1], length, '_ bv0 64', result)
        # else:
        #     return getAddResultList(exp1[:-1], exp2[:-1], length, '_ bv0 ' + str(length - 64), result)
        return getAddResultList(exp1, exp2, length, '_ bv0 64', result, i + 1)
    elif 1 < i < l + 1:
        if i == l:
            carryout = carryoutBVADD(exp1[l - i + 1], exp2[l - i + 1], c, mod)
        else:
            carryout = carryoutBVADD(exp1[l - i + 1], exp2[l - i + 1], c, 64)
        element = opBVADD(opBVADD(exp1[l - i], exp2[l - i]), carryout)
        result.append(element)
        return getAddResultList(exp1, exp2, length, carryout, result, i + 1)
    elif i == l + 1:
        return result[::-1]


def getNegResultList(exp, length, c, result, i):
    l = int(math.ceil(float(length) / 64))
    mod = length % 64
    if i == 1:
        if length > 64:
            element = opBVADD(opBVNOT(exp[l - i]), '_ bv1 64')
        else:
            element = opBVADD(opBVNOT(exp[l - i]), '_ bv1 ' + str(length))
        result.append(element)
        # todo: chek
        # if length > 128:
        #     return getNegResultList(exp[:-1], length, '_ bv0 64', result)
        # else:
        #     return getNegResultList(exp[:-1], length, '_ bv0 ' + str(length - 64), result)
        return getNegResultList(exp, length, '_ bv0 64', result, i + 1)
    elif 1 < i < l + 1:
        if i == l:
            carryout = carryoutBVNEG(exp[len(exp) - 1], c, mod)
        else:
            carryout = carryoutBVNEG(exp[len(exp) - 1], c, 64)
        element = opBVADD(opBVNOT(exp[l - i]), carryout)
        result.append(element)
        return getNegResultList(exp, length, carryout, result, i + 1)
    elif i == l + 1:
        return result[::-1]


def getMulResultList(exp1, exp2, length):
    mulList = []
    for i, _ in enumerate(exp1):
        li = mulResultListGenerator(exp1, exp2, i, length)
        mulList.append(li)
    return mulList


def mulResultListGenerator(exp1, exp2, index, length):
    firstSegLength = 64 if length % 64 == 0 else length % 64
    exp10 = exp1[0]
    exp20 = exp2[0]
    if firstSegLength < 64:
        exp10 = opZeroExtend(64 - firstSegLength, exp1[0])
        exp20 = opZeroExtend(64 - firstSegLength, exp2[0])
    i = index
    j = len(exp1) - 1
    result = None
    while i <= len(exp1) - 1:
        if i == 0 and j == 0:
            tmp = opBVMUL(exp10, exp20)
        elif i == 0:
            tmp = opBVMUL(exp10, exp2[j])
        elif j == 0:
            tmp = opBVMUL(exp1[i], exp20)
        else:
            tmp = opBVMUL(exp1[i], exp2[j])
        if result is None:
            result = tmp
        else:
            result = opBVADD(tmp, result)
        j -= 1
        i += 1
    i = index + 1
    j = len(exp1) - 1
    while i <= len(exp1) - 1:
        if result is None:
            result = carryoutBVMUL(exp1[i], exp2[j])
        else:
            result = opBVADD(carryoutBVMUL(exp1[i], exp2[j]), result)
        i += 1
        j -= 1
    if index == 0:
        if firstSegLength < 64:
            result = opExtract(firstSegLength - 1, 0, result)
    return result


def getSubResultList(exp1, exp2, length, c, result, i):
    l = int(math.ceil(float(length) / 64))
    mod = length % 64
    # print(l, i)
    if i == 1:
        element = opBVADD(exp1[l - i], opBVNEG(exp2[l - i]))
        result.append(element)
        # if length > 128:
        #     return getSubResultList(exp1, exp2, length, '_ bv0 64', result, i + 1)
        # else:
        #     return getSubResultList(exp1, exp2, length, '_ bv0 ' + str(length - 64), result, i + 1)
        return getSubResultList(exp1, exp2, length, '_ bv0 64', result, i + 1)
    elif l + 1 > i > 1:
        if i == l:
            carryout = carryoutBVSUB(exp1[l - i + 1], exp2[l - i + 1], c, mod)
        else:
            carryout = carryoutBVSUB(exp1[l - i + 1], exp2[l - i + 1], c, 64)
        element = opBVADD(opBVADD(exp1[l - i], opBVNEG(exp2[l - i])), carryout)
        result.append(element)
        return getSubResultList(exp1, exp2, length, carryout, result, i + 1)
    elif i == l + 1:
        return result[::-1]


def getNotResultList(exp, length):
    result = []
    l = int(math.ceil(float(length) / 64))
    for i in range(0, l):
        result.append(opBVNOT(exp[i]))
    return result


def getAndResultList(exp1, exp2, length):
    result = []
    l = int(math.ceil(float(length) / 64))
    for i in range(0, l):
        result.append(opBVAND(exp1[i], exp2[i]))
    return result


def getOrResultList(exp1, exp2, length):
    result = []
    l = int(math.ceil(float(length) / 64))
    for i in range(0, l):
        result.append(opBVOR(exp1[i], exp2[i]))
    return result


def getLshrResultList(exp, shiftNum, length):
    j = shiftNum // 64
    k = shiftNum % 64
    l = int(math.ceil(float(length) / 64))
    result = []
    for i in range(0, l):
        if 0 <= i <= j - 1:
            e = "_ bv0 64"
        elif i == j:
            e = opBVLSHR(exp[0], '_ bv' + str(k) + ' ' + str(64 if length % 64 == 0 else length % 64))
        else:
            e = opConcat(opExtract(k - 1, 0, exp[i - j - 1]),
                         opExtract(63, k, exp[i - j]))
        result.append(e)
    return result


def getShlResultList(exp, shiftNum, length):
    j = shiftNum // 64
    k = shiftNum % 64
    l = int(math.ceil(float(length) / 64))
    m = 64 if length % 64 == 0 else length % 64
    result = []
    for i in range(0, l):
        if l - j - 1 < i <= l - 1:
            e = "_ bv0 64"
        elif i == l - j - 1:
            e = opBVSHL(exp[i + j], '_ bv' + str(k) + ' 64')
        else:
            if i == 0:
                e = opConcat(opExtract(m - 1 - k, 0, exp[i + j]),
                             opExtract(63, 64 - shiftNum, exp[i + j + 1]))
            else:
                e = opConcat(opExtract(63 - k, 0, exp[i + j]),
                             opExtract(63, 64 - shiftNum, exp[i + j + 1]))
        result.append(e)
    return result


def getNandResultList(exp1, exp2, length):
    result = []
    l = int(math.ceil(float(length) / 64))
    for i in range(0, l):
        result.append(opBVNAND(exp1[i], exp2[i]))
    return result


def getNorResultList(exp1, exp2, length):
    result = []
    l = int(math.ceil(float(length) / 64))
    for i in range(0, l):
        result.append(opBVNOR(exp1[i], exp2[i]))
    return result


def getXorResultList(exp1, exp2, length):
    result = []
    l = int(math.ceil(float(length) / 64))
    for i in range(0, l):
        result.append(opBVXOR(exp1[i], exp2[i]))
    return result


def getXnorResultList(exp1, exp2, length):
    result = []
    l = int(math.ceil(float(length) / 64))
    for i in range(0, l):
        result.append(opBVXNOR(exp1[i], exp2[i]))
    return result


def getAshrResultList(exp, shiftNum, length):
    j = shiftNum // 64
    k = shiftNum % 64
    l = int(math.ceil(float(length) / 64))
    result = []
    for i in range(0, l):
        if 0 <= i <= j - 1:
            e = "_ bv0 64"
        elif i == j:
            e = opBVASHR(exp[0], '_ bv' + str(k) + ' 64')
        else:
            e = opConcat(opExtract(k - 1, 0, exp[i - j - 1]),
                         opExtract(63, k, exp[i - j]))
        result.append(e)
    return result


def getUltResultList(exp1, exp2, length, result):
    l = int(math.ceil(float(length) / 64))
    if len(exp1) == l:
        result = opBVULT(exp1[0], exp2[0])
        return getUltResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) > 0:
        result = opOr([opBVULT(exp1[0], exp2[0]), opAnd([opEqual(exp1[0], exp2[0]), result], 2)], 2)
        return getUltResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) == 0:
        return result


def getUleResultList(exp1, exp2, length, result):
    l = int(math.ceil(float(length) / 64))
    if len(exp1) == l:
        result = opBVULE(exp1[0], exp2[0])
        return getUleResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) > 0:
        result = opOr([opBVULT(exp1[0], exp2[0]), opAnd([opEqual(exp1[0], exp2[0]), result], 2)], 2)
        return getUleResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) == 0:
        return result


def getUgtResultList(exp1, exp2, length, result):
    l = int(math.ceil(float(length) / 64))
    if len(exp1) == l:
        result = opBVUGT(exp1[0], exp2[0])
        return getUgtResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) > 0:
        result = opOr([opBVUGT(exp1[0], exp2[0]), opAnd([opEqual(exp1[0], exp2[0]), result], 2)], 2)
        return getUgtResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) == 0:
        return result


def getUgeResultList(exp1, exp2, length, result):
    l = int(math.ceil(float(length) / 64))
    if len(exp1) == l:
        result = opBVUGE(exp1[0], exp2[0])
        return getUgeResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) > 0:
        result = opOr([opBVUGT(exp1[0], exp2[0]), opAnd([opEqual(exp1[0], exp2[0]), result], 2)], 2)
        return getUgeResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) == 0:
        return result


def getSltResultList(exp1, exp2, length, result):
    l = int(math.ceil(float(length) / 64))
    if len(exp1) == l:
        result = opBVULT(exp1[0], exp2[0])
        return getUgeResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) > 1:
        result = opOr([opBVULT(exp1[0], exp2[0]), opAnd([opEqual(exp1[0], exp2[0]), result], 2)], 2)
        return getUgeResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) == 1:
        result = opOr([opBVSLT(exp1[0], exp2[0]), opAnd([opEqual(exp1[0], exp2[0]), result], 2)], 2)
        return getUgeResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) == 0:
        return result


def getSleResultList(exp1, exp2, length, result):
    l = int(math.ceil(float(length) / 64))
    if len(exp1) == l:
        result = opBVULE(exp1[0], exp2[0])
        return getSleResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) > 1:
        result = opOr([opBVULT(exp1[0], exp2[0]), opAnd([opEqual(exp1[0], exp2[0]), result], 2)], 2)
        return getSleResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) == 1:
        result = opOr([opBVSLT(exp1[0], exp2[0]), opAnd([opEqual(exp1[0], exp2[0]), result], 2)], 2)
        return getSleResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) == 0:
        return result


def getSgtResultList(exp1, exp2, length, result):
    l = int(math.ceil(float(length) / 64))
    if len(exp1) == l:
        result = opBVUGT(exp1[0], exp2[0])
        return getSgtResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) > 1:
        result = opOr([opBVUGT(exp1[0], exp2[0]), opAnd([opEqual(exp1[0], exp2[0]), result], 2)], 2)
        return getSgtResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) == 1:
        result = opOr([opBVSGT(exp1[0], exp2[0]), opAnd([opEqual(exp1[0], exp2[0]), result], 2)], 2)
        return getSgtResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) == 0:
        return result


def getSgeResultList(exp1, exp2, length, result):
    l = int(math.ceil(float(length) / 64))
    if len(exp1) == l:
        result = opBVUGE(exp1[0], exp2[0])
        return getSgeResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) > 1:
        result = opOr([opBVUGT(exp1[0], exp2[0]), opAnd([opEqual(exp1[0], exp2[0]), result], 2)], 2)
        return getSgeResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) == 1:
        result = opOr([opBVSGT(exp1[0], exp2[0]), opAnd([opEqual(exp1[0], exp2[0]), result], 2)], 2)
        return getSgeResultList(exp1[1:], exp2[1:], length, result)
    elif len(exp1) == 0:
        return result


def getRotateLResultList(exp, rotateNum, length):
    j = rotateNum // 64
    k = rotateNum % 64
    l = int(math.ceil(float(length) / 64))
    m = length % 64
    result = []
    for i in range(0, l):
        if 0 <= i <= l - 2 - j:
            e = opConcat(opExtract(63 - k, 0, exp[i + j]), opExtract(63, 64 - k, exp[i + j + 1]))
        elif i == l - 1 - j:
            e = opConcat(opExtract(63 - k, 0, exp[l - 1]), opExtract(m - 1, m - k, exp[0]))
        else:
            e = opConcat(opExtract(63 - k, 0, exp[i + j - l]), opExtract(63, 64 - k, exp[i + j - l + 1]))
        result.append(e)
    return result


def getRotateRResultList(exp, rotateNum, length):
    j = rotateNum // 64
    k = rotateNum % 64
    l = int(math.ceil(float(length) / 64))
    m = length % 64
    result = []
    for i in range(0, l):
        if 0 <= i <= j - 1:
            e = opConcat(opExtract(k - 1, 0, exp[i + l - j - 1]), opExtract(63, k, exp[i + l - j]))
        elif i == j:
            e = opConcat(opExtract(k - 1, 0, exp[l - 1]), opExtract(m - 1, k, exp[0]))
        else:
            e = opConcat(opExtract(k - 1, 0, exp[i - j - 1]), opExtract(63, k, exp[i - j]))
        result.append(e)
    return result


def getConcatResultList(exp1, exp2, length1, length2):
    n = length1 // 64
    m = length1 % 64
    j = length2 // 64
    k = length2 % 64
    l1 = int(math.ceil(float(length1) / 64))
    l2 = int(math.ceil(float(length2) / 64))
    result = []
    if j == 0 and n == 0:
        if k + m <= 64:
            e = opConcat(exp1[0], exp2[0])
            result.append(e)
        else:
            result.append(opExtract(m - 1, 64 - k, exp1[0]))
            result.append(opConcat(opExtract(63 - k, 0, exp1[0]), opExtract(k - 1, 0, exp2[0])))
    elif n == 0:
        if k + m > 64:
            for i in range(0, j + 2):
                if i == 0:
                    e = opExtract(m - 1, 64 - k, exp1[0])
                elif i == 1:
                    e = opConcat(opExtract(63 - k, 0, exp1[0]), opExtract(k - 1, 0, exp2[0]))
                else:
                    e = exp2[i - 1]
                result.append(e)
        elif k == 0:
            for i in range(0, j + 1):
                if i == 0:
                    e = exp1[0]
                else:
                    e = exp2[i - 1]
                result.append(e)
        elif k + m > 0:
            for i in range(0, j + 1):
                if i == 0:
                    e = opConcat(exp1[0], exp2[0])
                else:
                    e = exp2[i]
                result.append(e)
    elif j == 0:
        if k + m > 64:
            for i in range(0, n + 2):
                if i == n + 1:
                    e = opConcat(opExtract(63 - k, 0, exp1[i - 1]), exp2[0])
                elif i == 0:
                    e = opExtract(m - 1, 64 - k, exp1[0])
                else:
                    e = opConcat(opExtract(63 - k, 0, exp1[i - 1]), opExtract(63, 64 - k, exp1[i]))
                result.append(e)
        elif m == 0:
            for i in range(0, n + 1):
                if i == n:
                    e = opConcat(opExtract(63 - k, 0, exp1[i - 1]), exp2[0])
                elif i == 0:
                    e = opExtract(63, 64 - k, exp1[0])
                else:
                    e = opConcat(opExtract(63 - k, 0, exp1[i - 1]), opExtract(63, 64 - k, exp1[i]))
                result.append(e)
        elif k + m > 0:
            for i in range(0, n + 1):
                if i == n:
                    e = opConcat(opExtract(63 - k, 0, exp1[i]), exp2[0])
                elif i == 0:
                    e = opConcat(exp1[0], opExtract(63, 64 - k, exp1[1]))
                else:
                    e = opConcat(opExtract(63 - k, 0, exp1[i]), opExtract(63, 64 - k, exp1[i + 1]))
                result.append(e)
    elif k + m > 64:
        for i in range(0, n + j + 2):
            if i == 0:
                e = opExtract(m - 1, 64 - k, exp1[0])
            elif 1 <= i <= n:
                e = opConcat(opExtract(63 - k, 0, exp1[i - 1]), opExtract(k - 1, 0, exp1[i]))
            elif i == n + 1:
                e = opConcat(opExtract(63 - k, 0, exp1[i - 1]), exp2[0])
            else:
                e = exp2[i - n - 1]
            result.append(e)
    elif k + m > 0:
        for i in range(0, n + j + 1):
            if i == 0:
                e = opConcat(exp1[0], opExtract(63, 64 - k, exp1[1]))
            elif 1 <= i <= n - 1:
                e = opConcat(opExtract(63 - k, 0, exp1[i - 1]), opExtract(k - 1, 0, exp1[i]))
            elif i == n:
                e = opConcat(opExtract(63 - k, 0, exp1[i]), exp2[0])
            else:
                e = exp2[i - n]
            result.append(e)
    else:
        for i in range(0, n + j):
            if 0 <= i <= n - 1:
                e = exp1[i]
            else:
                e = exp2[i - n]
            result.append(e)
    return result


def getRepeatResultList(exp, length, times):
    result = ()
    if length * times <= 64:
        e = opRepeat(exp, times)
        return e
    for i in range(1, times + 1):
        if i == 1:
            result = (exp, length)
        else:
            result = (getConcatResultList(result[0], exp, result[1], length), result[1] + length)
    return result[0]


def getExtractResultList(exp, left, right, length):
    # print("extract", left, right, length)
    l = int(math.ceil(float(length) / 64))
    d1 = left % 64 + 1
    d2 = right % 64
    d3 = (right + 1) // 64
    gap = left - right + 1
    j = int(math.ceil(float(gap) / 64))
    result = []
    if left == right:
        interval = int(math.ceil(float(right + 1) / 64))
        index = l - interval
        # print("left=right, exp=", exp, "index=", index)
        result.append(opExtract(d2, d2, exp[index]))
    elif d1 > d2:
        for i in range(0, j):
            if i == 0:
                e = opExtract(d1 - 1, d2, exp[l - d3 - j])
            else:
                if d2 == 0:
                    e = opExtract(63, d2, exp[l - d3 + i - j])
                else:
                    e = opConcat(opExtract(d2 - 1, 0, exp[l - d3 + i - 1 - j]), opExtract(63, d2, exp[l - d3 + i - j]))
            result.append(e)
    else:
        for i in range(0, j):
            if i == 0:
                if d1 == 0:
                    e = opExtract(63, d2, exp[l - d3 - j])
                else:
                    e = opConcat(opExtract(d1 - 1, 0, exp[l - d3 - j - 1]), opExtract(63, d2, exp[l - d3 - j]))
            else:
                e = opConcat(opExtract(d2 - 1, 0, exp[l - d3 + i - 1 - j]), opExtract(63, d2, exp[l - d3 + i - j]))
            result.append(e)
    return result


def getZeroExtendResultList(exp, length, extendNum):
    n = length // 64
    m = length % 64
    j = extendNum // 64
    k = extendNum % 64
    result = []
    if k + m > 64:
        for i in range(0, n + j + 2):
            if i == 0:
                e = "_ bv0 " + str((k + m) % 64)
            elif 1 <= i <= j:
                e = "_ bv0 64"
            elif i == j + 1:
                e = opConcat("_ bv0 " + str(64 - m), exp[0])
            else:
                e = exp[i - j - 1]
            result.append(e)
    elif k + m > 0:
        for i in range(0, n + j + 1):
            if i == 0:
                e = "_ bv0 " + str(k + m)
            elif 1 <= i <= j - 1:
                e = "_ bv0 64"
            elif i == j:
                if m > 0:
                    e = opConcat("_ bv0 " + str(64 - m), exp[0])
                else:
                    e = "_ bv0 64"
            else:
                if m == 0:
                    e = exp[i - j - 1]
                else:
                    e = exp[i - j]
            result.append(e)
    else:
        for i in range(0, n + j):
            if 0 <= i <= j - 1:
                e = "_ bv0 64"
            else:
                e = exp[i - j]
            result.append(e)
    return result


def getSignExtendResultList(exp, length, extendNum):
    n = length // 64
    m = length % 64
    j = extendNum // 64
    k = extendNum % 64
    s = opExtract(m - 1, m - 1, exp[0])
    result = []
    if k + m > 64:
        for i in range(0, n + j + 2):
            if i == 0:
                e = getRepeatResultList(s, 1, (k + m) % 64)
            elif 1 <= i <= j:
                e = getRepeatResultList(s, 1, 64)
            elif i == j + 1:
                e = opConcat(getRepeatResultList(s, 1, 64 - m), exp[0])
            else:
                e = exp[i - j - 1]
            result.append(e)
    elif k + m > 0:
        for i in range(0, n + j + 1):
            if i == 0:
                e = getRepeatResultList(s, 1, k + m)
            elif 1 <= i <= j - 1:
                e = getRepeatResultList(s, 1, 64)
            elif i == j:
                if m > 0:
                    e = opConcat(getRepeatResultList(s, 1, 64 - m), exp[0])
                else:
                    e = getRepeatResultList(s, 1, 64)
            else:
                if m > 0:
                    e = exp[i - j]
                else:
                    e = exp[i - j - 1]
            result.append(e)
    else:
        for i in range(0, n + j):
            if 0 <= i <= j - 1:
                e = getRepeatResultList(s, 1, 64)
            else:
                e = exp[i - j]
            result.append(e)
    return result
